<?php
namespace controllers;

class MainController extends \core\base\Controller
{
    public function actionIndex()
    {
        $this->render('posts/index');
    }
    
    public function actionNotfound()
    {
        header("HTTP/1.0 404 Not Found");
        $this->render('not-found');
    }
}

