<?php
namespace core;

class Config extends base\Singleton
{
    public function init()
    {
        $this->setAttributes(require $_SERVER['DOCUMENT_ROOT'] . '/config/config.php');
    }
    
    public function __construct()
    {
        parent::__construct();
    }
}

