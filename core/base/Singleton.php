<?php
namespace core\base;

class Singleton extends Object
{
    private static $instance = [];
    
    public static function getInstance()
    {
        $className = get_called_class();
        if(empty(self::$instance[$className]) || is_null(self::$instance[$className]))
            self::$instance[$className] = new $className;
        return self::$instance[$className];
    }
    
    public function __construct() {
        $this->init();
    }
    
    public function init() {}
}

