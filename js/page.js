$(function(){
    $.ajaxSetup({
        beforeSend: function()
        {
          $('.posts').html('<div class="col-lg-10 text-center"><img src="/images/preloader.gif" /></div>');
        }
    });
    loadPosts();
    $(document).on('click','.view-post',function(e){
        e.preventDefault();
        viewPost($(this).attr('href'));
    });
    $(document).on('click','.back',function(e){
        e.preventDefault();
        loadPosts();
    });
})

function viewPost(href)
{
    $.ajax({
        url: href,
        success: function(data)
        {
            $('.posts').html(data);
        }
    });
}

function loadPosts()
{
    $.ajax({
        url: '/posts/list',
        success: function(data)
        {
            $('.posts').html(data);
        }
    });
}