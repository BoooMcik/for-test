<?php
namespace core\base;

class View extends Object
{
    public static function render($template,$paramethers)
    {
        $dir = $_SERVER['DOCUMENT_ROOT'] . '/views/';
        if(!file_exists($dir . $template))
        {
            if(count($paramethers > 0))
                extract($paramethers);
            include $dir . $template . '.php';
        } else {
            Controller::redirect404();
        }
    }
}
