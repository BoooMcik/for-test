<?php
namespace controllers;

class PostsController extends \core\base\Controller
{
    public function actionList()
    {
        $posts = \models\Posts::findAll();

        $this->render('posts/list',[
            'posts' => $posts
        ]);
    }
    
    public function actionView()
    {
        $post = \models\Posts::findOne(['id' => $_GET['id']]);
        if(!$post)
            $this->redirect404();
        
        $this->render('/posts/view',[
            'post' => $post
        ]);
    }
}