<div class="panel">
    <a href="/main/index" class="back"><< Go Back</a>
</div>
<div class="panel col-lg-10 post view">
    <div class="panel-head">
        <h1 class="title"><?=$post->title?></h1>
        <div class="row created">Updated at: <?=$post->getUpdatedDate()?></div>
    </div>
    <div class="panel-body text">
        <?=$post->text?>
    </div>
</div>