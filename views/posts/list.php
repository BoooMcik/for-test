<?php foreach($posts as $post):?>
    <div class="panel col-lg-10 post">
        <div class="panel-head">
            <a class="view-post title" href="/posts/view/?id=<?=$post->id?>"><?=$post->title?></a>
            <div class="row created">Updated at: <?=$post->getUpdatedDate()?><div>
        </div></div>
        </div>
        <div class="panel-body text">
            <?=$post->text?>
        </div>
    </div>
<?php endforeach;?>