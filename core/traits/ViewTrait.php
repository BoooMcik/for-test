<?php
namespace core\traits;

trait ViewTrait
{
    public function render($template,$parameters)
    {
        return \core\base\View::render($template, $parameters);
    }
}
