<?php
namespace core;

Class Db extends base\Singleton
{
    private function connect()
    {
        $config = Config::getInstance();

        try {
            $this->connection = new \PDO('mysql:dbname=' . $config->db['database'] . ';host=' . $config->db['host'], $config->db['user'], $config->db['password']);
        }
        catch (PDOException $e) {
            echo 'Connection error: ' . $e->getMessage();
        }
        
        return $this;
    }
    
    public function init()
    {
        $this->connect();
    }
    
    public function execute($params = [])
    {
        $this->statement->execute($params);
        
        return $this->statement;
    }
    
    public function createCommand($sql)
    {
        $this->statement = $this->connection->prepare($sql);
        return $this;
    }
}

