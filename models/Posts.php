<?php
namespace models;

class Posts extends \core\base\Model
{
    protected static $tablename = 'posts';
    
    protected $fields = [
        'title' => ['required' => true],
        'text' => ['required' => true],
    ];
    
    protected function beforeSave() {
        $this->lastUpdate = time();
    }
    
    public function getUpdatedDate()
    {
        return date('m-d-Y H:i:s',$this->lastUpdate);
    }
}

