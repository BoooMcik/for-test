<?php
namespace core\base;
class Object 
{
    private $properties = [];
    
    public function __get($name) {
        if(isset($this->properties[$name]))
            return $this->properties[$name];
        return null;
    }
    
    public function __set($name, $value) 
    {
        $this->properties[$name] = $value;
    }
    
    public function __construct($parameters)
    {
        foreach($parameters as $key => $value)
            $this->properties[$key] = $value;
    }
    
    public function notExists($field)
    {
        return $this->__get($field) !== null && strlen($this->__get($field)) > 0 ? false : true;
    }
    
    public function exists($field)
    {
        return $this->__get($field) !== null && strlen($this->__get($field)) > 0 ? true : false;
    }
    
    public function getAttributes()
    {
        return $this->properties;
    }
    
    public function clear()
    {
        unset($this->properties);
    }
    
    protected function setAttributes($attributes)
    {
        $this->properties = $attributes;
    }
    
}

