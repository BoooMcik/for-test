<?php
namespace core\base;

class Model extends Object
{
    protected static $tablename = '';
    protected $fields = [];
    private $errorMessages = [];
    private $changedAttributes = [];
    
    public function __set($name, $value) 
    {
        if($this->exists('id'))
            $this->changedAttributes[$name] = $value;
        parent::__set($name, $value);
    }
    
    public static function findAll($criterea = [])
    {
        $result = self::getByParams($criterea);
        $objects = [];
        $className = get_called_class();
        if($result === false)
            return false;
        foreach($result as $item)
            $objects[] = new $className($item);
        return $objects;
    }
    
    public static function findOne($criterea = [])
    {
        if(empty($criterea))
            throw new \Exception ('Can not find one object without criterea');
        $result = self::getByParams($criterea, 1);
        $className = get_called_class();
        if($result === false)
            return false;
        return new $className($result);
    }
    
    public static function getByParams($criterea = [], $limit = 0)
    {
        $sql = 'SELECT * FROM ' . static::$tablename;
        if(count($criterea) > 0)
            $sql .= ' WHERE ' . self::prepareCriterea($criterea);
        if($limit == 1)
            $sql .= ' LIMIT 1';
       
        $result = \core\Db::getInstance()->createCommand($sql)->execute();
        
        if($result->rowCount() == 0)
            return false;
        
        return $limit == 1 ? $result->fetch(\PDO::FETCH_ASSOC) : $result->fetchAll(\PDO::FETCH_ASSOC);
    }
    
    public function validate()
    {
        foreach($this->fields as $field => $config)
            $this->validateRequired($field, $config);
        return count($this->getErrorMessages()) == 0 ? true : false;
    }
    
    public function save()
    {
        $this->beforeSave();
        if($this->validate())
        {
            if($this->id)
            {
                
                $result = $this->update();
            } else
                $result = $this->insert();
            $this->afterSave();
            return $result;
        }
        return $this->getErrorMessages();
    }
    
    private function getErrorMessages()
    {
        return $this->errorMessages;
    }
    
    private function validateRequired($field,$config)
    {
        $required = isset($config['required']) || $config['required'] == true ? true : false;
        if($required && $this->notExists($field))
            $this->errorMessages[] = 'Field "' . $field . '" must be required';
        return $this;
    }
    
    private function insert()
    {
        $sql = 'INSERT INTO ' . static::$tablename . '(' . $this->prepareColumnsForInsert() . ') VALUES(' . $this->prepareValuesForInsert() . ')';
        \core\Db::getInstance()->createCommand($sql)->execute();
        return $this;
    }
    
    private function update()
    {
        if(empty($this->changedAttributes))
            return $this;
        $sql = 'UPDATE ' . static::$tablename . ' SET ' . $this->prepareColumnsForUpdate() . ' WHERE id=' . $this->id;
        \core\Db::getInstance()->createCommand($sql)->execute();
        return $this;
    }
    
    protected function afterSave()
    {
        if(count($this->changedAttributes))
            $this->changedAttributes = [];
    }
    
    protected function beforeSave()
    {
        
    }
    
    private function prepareColumnsForUpdate()
    {
        $string = '';
        foreach($this->changedAttributes as $attribute => $value)
        {
            if(strlen($string) > 0)
                $string .= ',';
            $string .= '`' . $attribute . '` = ' . (is_int($value) ? $value : '"' . $value . '"');
        }
        
        return $string;
    }
    
    private static function prepareCriterea($criterea)
    {
        $sqlCriterea = '';
        foreach($criterea as $parameter => $value)
        {
            if(strlen($sqlCriterea) > 0)
                $sqlCriterea .= ' AND ';
            $value = is_int($value) ? $value : '"' . $value . '"';
            $sqlCriterea .= $parameter . ' = ' . $value;
        }
        
        return $sqlCriterea;
    }
    
    private function prepareColumnsForInsert()
    {
        $columns = array_keys($this->getAttributes());
        return self::prepareArray($columns);
    }
    
    private function prepareValuesForInsert()
    {
        $values = array_values($this->getAttributes());
        return self::prepareArray($values,true);
    }
    
    private static function prepareArray($array,$escape = false)
    {
        $string = '';
        foreach($array as $item)
        {
            if(strlen($string) > 0)
                $string .= ',';
            if($escape)
                $string .= is_int($item) ? $item : '"' . $item . '"';
            else
                $string .= '`' . $item . '`';
        }
        
        return $string;
    }
}
