<?php
namespace core\base;

class Controller extends Singleton
{
    use \core\traits\ViewTrait;
    
    private $controllerClass = '\controllers\MainController';
    private $action = 'actionIndex';
    
    public function init()
    {
        $this->prepareUri();
    }
    
    private function prepareUri()
    {
        preg_match_all('|([a-z]+)|',$_SERVER['REQUEST_URI'],$parts);
        if(empty($parts[1]))
            self::redirect404();
        if(count($parts[1]) > 0)
        {
            $this->controllerClass = '\\controllers\\' . ucfirst($parts[1][0]) . 'Controller';
            if(isset($parts[1][1]))
                $this->action = 'action' . ucfirst($parts[1][1]);
        }
    }
    
    public function run()
    {
        if(class_exists($this->controllerClass))
        {
            $controller = new $this->controllerClass;
            if(method_exists($controller, $this->action))
                $controller->{$this->action}();
            else
                self::redirect404();
        } else {
            self::redirect404();
        }
    }
    
    public static function redirect404()
    {
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: /main/notfound");
    }
}

